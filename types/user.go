package types

import (
	"errors"
	"fmt"
	"time"
)

type UnixTimestamp int64

type User struct {
	ID            string        `json:"id"`
	FirstName     string        `json:"first_name"`
	LastName      string        `json:"last_name"`
	Age           int32         `json:"age"`
	RecordingDate UnixTimestamp `json:"recording_date"`
}

func (j *UnixTimestamp) Scan(value interface{}) error {

	timeValue, ok := value.(time.Time)
	if !ok {
		return errors.New(fmt.Sprint("Failed to unmarshal value:", value))
	}

	*j = UnixTimestamp(timeValue.Unix())
	return nil
}
