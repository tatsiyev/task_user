up:
	docker compose up -d --build

test:
	@make test-create
	@echo
	@make test-pagination
	@echo
	@make test-filter
	@echo

test-create:
	@curl --location 'localhost:8080/users' \
	--header 'Content-Type: application/json' \
	--data '{"first_name": "first_name","last_name": "last_name","age": 30}'

test-pagination:
	@curl --location 'localhost:8080/users/paginated' \
	--header 'Content-Type: application/json' \
	--data '{ "page": 1, "page_size": 5 }'

test-filter:
	@curl --location 'localhost:8080/users/filter' \
	--header 'Content-Type: application/json' \
	--data '{ "from_recording": 1708008757, "from_age": 1 }'