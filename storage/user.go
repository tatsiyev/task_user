package storage

import (
	"time"

	"github.com/google/uuid"
	"gitlab.com/tatsiyev/task_user/types"
	"gorm.io/gorm"
)

type UserStorageInterface interface {
	CreateUser(firstName, lastName string, age int32) (*types.User, error)
	PaginatedUsers(page, pageSize int) ([]*types.User, int64, error)
	FilterUsers(fromAge, toAge, fromRecording, toRecording *int) ([]*types.User, int64, error)
}

type UserStorage struct {
	db *gorm.DB
}

func NewUserStorage(db *gorm.DB) UserStorageInterface {
	return &UserStorage{db: db}
}

func (s *UserStorage) CreateUser(firstName, lastName string, age int32) (*types.User, error) {
	user := &types.User{
		ID:        uuid.NewString(),
		FirstName: firstName,
		LastName:  lastName,
		Age:       age,
	}

	db := s.db.Select("id", "last_name", "first_name", "age").Create(&user)
	if db.Error != nil {
		return nil, db.Error
	}

	db = s.db.Where("id = ?", user.ID).First(&user)
	if db.Error != nil {
		return nil, db.Error
	}
	return user, nil
}

func (s *UserStorage) PaginatedUsers(page, pageSize int) ([]*types.User, int64, error) {
	var list []*types.User = make([]*types.User, 0)
	var count int64

	db := s.db.Limit(pageSize).Offset((page - 1) * pageSize).Find(&list)
	if db.Error != nil {
		return nil, 0, db.Error
	}

	db = s.db.Model(&types.User{}).Count(&count)
	if db.Error != nil {
		return nil, 0, db.Error
	}

	return list, count, nil
}

func (s *UserStorage) FilterUsers(fromAge, toAge, fromRecording, toRecording *int) ([]*types.User, int64, error) {
	var result []*types.User = make([]*types.User, 0)
	var count int64

	query := s.db.Model(&types.User{})

	if fromAge != nil {
		query = query.Where("age > ?", fromAge)
	}
	if toAge != nil {
		query = query.Where("age < ?", toAge)
	}

	if fromRecording != nil {
		t := time.Unix(int64(*fromRecording), 0)
		query = query.Where("recording_date > ?", t)
	}
	if toRecording != nil {
		t := time.Unix(int64(*toRecording), 0)
		query = query.Where("recording_date < ?", t)
	}

	db := query.Find(&result)
	if db.Error != nil {
		return nil, 0, db.Error
	}

	db = query.Count(&count)
	if db.Error != nil {
		return nil, 0, db.Error
	}

	return result, count, nil

}
