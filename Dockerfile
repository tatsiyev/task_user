FROM golang:1.21 as builder

WORKDIR /app
COPY go.mod go.sum ./
RUN go mod download

COPY . .

RUN CGO_ENABLED=0 go build -o user-service

FROM scratch

WORKDIR /app
COPY --from=builder /app/user-service .
COPY migrations/sql /app/migrations/sql
COPY config.yml .

CMD ["/app/user-service", "migrate", "run"]
