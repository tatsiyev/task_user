package server

import (
	"database/sql"
	"fmt"
	"time"

	_ "github.com/lib/pq"
	"gitlab.com/tatsiyev/task_user/migrations"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func UpMigration() error {
	config, e := readConfig(configFileName)
	if e != nil {
		return e
	}

	postgresDB, e := sql.Open("postgres", fmt.Sprintf(
		"user=%s password=%s host=%s port=%s dbname=%s sslmode=%s",
		config.Database.User,
		config.Database.Pass,
		config.Database.Host,
		config.Database.Port,
		config.Database.DBName,
		config.Database.Mode))

	if e != nil {
		return e
	}

	e = migrations.Migrate(postgresDB)
	if e != nil {
		return e
	}
	return nil
}

func initDatabase(config *Config) (db *gorm.DB, e error) {

	postgresDB, e := sql.Open("postgres",
		fmt.Sprintf(
			"user=%s password=%s host=%s port=%s dbname=%s sslmode=%s",
			config.Database.User,
			config.Database.Pass,
			config.Database.Host,
			config.Database.Port,
			config.Database.DBName,
			config.Database.Mode))
	if e != nil {
		return nil, e
	}

	errorWait := make(chan error)

	go func() {
		errorWait <- postgresDB.Ping()
	}()

	select {
	case e = <-errorWait:
		if e != nil {
			return nil, e
		}
	case <-time.After(5 * time.Second):
		e = fmt.Errorf("failed to connect to database")
		return nil, e
	}

	if e != nil {
		return nil, e
	}

	db, e = gorm.Open(postgres.New(postgres.Config{Conn: postgresDB}), &gorm.Config{})
	if e != nil {
		return nil, e
	}

	return db, nil
}
