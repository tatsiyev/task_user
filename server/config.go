package server

import (
	"os"

	"gopkg.in/yaml.v2"
)

type Config struct {
	Mode string

	Server struct {
		HttpPort string `yaml:"http_port"`
	} `yaml:"server"`

	Database struct {
		UpMigrations bool `yaml:"up_migrations"`
		Host         string
		Port         string
		User         string
		Pass         string
		DBName       string
		Mode         string
	}
}

func readConfig(file string) (*Config, error) {
	buf, e := os.ReadFile(file)
	if e != nil {
		return nil, e
	}

	res := &Config{}
	e = yaml.Unmarshal(buf, res)

	return res, e
}
