package server

import (
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

const configFileName = "./config.yml"

type Server struct {
	ORM  *gorm.DB
	Http *gin.Engine
}

func Init() (server *Server, e error) {
	server = &Server{}

	config, e := readConfig(configFileName)
	if e != nil {
		return server, e
	}

	server.ORM, e = initDatabase(config)
	if e != nil {
		return server, e
	}

	server.Http = gin.New()

	return server, e
}
