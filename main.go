package main

import (
	"fmt"
	"os"

	"gitlab.com/tatsiyev/task_user/api"
	"gitlab.com/tatsiyev/task_user/server"
	"gitlab.com/tatsiyev/task_user/storage"
)

func main() {
	cmdList := []string{"run"}
	if len(os.Args) > 1 {
		cmdList = os.Args[1:]
	}

	for _, cmd := range cmdList {
		switch cmd {
		case "run":
			run()
		case "migrate":
			migrate()
		}
	}
}

func run() {
	srv, err := server.Init()
	if err != nil {
		fmt.Println(err)
		return
	}
	userStorage := storage.NewUserStorage(srv.ORM)

	api.NewUserRoutes(srv.Http, userStorage)

	err = srv.Http.Run()
	fmt.Println(err)
}

func migrate() {
	err := server.UpMigration()
	if err != nil {
		fmt.Println(err)
	}
}
