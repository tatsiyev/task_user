-- +goose Up
-- +goose StatementBegin
CREATE TABLE IF NOT EXISTS public.users(
	id uuid NOT NULL DEFAULT uuid_generate_v4(),
    first_name varchar(255) NOT NULL,
	last_name varchar(255) NOT NULL,

	age int NOT NULL,
    recording_date timestamptz(0) NOT NULL DEFAULT now(),
	
	CONSTRAINT users_pk PRIMARY KEY (id)
);

-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE public.users;
-- +goose StatementEnd
