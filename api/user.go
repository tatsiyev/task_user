package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/tatsiyev/task_user/storage"
)

type ResponseError struct {
	Error string `json:"error"`
}

type PaginatedResponse struct {
	TotalCount int64       `json:"total_count"`
	Entries    interface{} `json:"entries"`
}

type UserHTTP struct {
	UserStorage storage.UserStorageInterface
}

type CreateUserRequest struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Age       int32  `json:"age"`
}

func (u *UserHTTP) createUser(ctx *gin.Context) {
	var requestData CreateUserRequest

	err := ctx.Bind(&requestData)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, ResponseError{Error: err.Error()})
		return
	}

	user, err := u.UserStorage.CreateUser(requestData.FirstName, requestData.LastName, requestData.Age)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, ResponseError{Error: err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, user)
}

type PaginatedUsersRequest struct {
	Page     int `json:"page"`
	PageSize int `json:"page_size"`
}

func (u *UserHTTP) paginateUser(ctx *gin.Context) {
	var requestData PaginatedUsersRequest

	err := ctx.Bind(&requestData)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, ResponseError{Error: err.Error()})
		return
	}

	users, count, err := u.UserStorage.PaginatedUsers(requestData.Page, requestData.PageSize)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, ResponseError{Error: err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, PaginatedResponse{TotalCount: count, Entries: users})
}

type FilterUsersRequest struct {
	FromAge       *int `json:"from_age"`
	ToAge         *int `json:"to_age"`
	FromRecording *int `json:"from_recording"`
	ToRecording   *int `json:"to_recording"`
}

func (u *UserHTTP) filterUser(ctx *gin.Context) {
	var requestData FilterUsersRequest

	err := ctx.Bind(&requestData)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, ResponseError{Error: err.Error()})
		return
	}

	users, count, err := u.UserStorage.FilterUsers(requestData.FromAge, requestData.ToAge, requestData.FromRecording, requestData.ToRecording)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, ResponseError{Error: err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, PaginatedResponse{TotalCount: count, Entries: users})
}

func NewUserRoutes(g *gin.Engine, userStorage storage.UserStorageInterface) {
	userHTTP := &UserHTTP{UserStorage: userStorage}

	g.Use(func(ctx *gin.Context) {
		// ctx.Header("", "")
	})

	g.POST("/users", userHTTP.createUser)
	g.POST("/users/paginated", userHTTP.paginateUser)
	g.POST("/users/filter", userHTTP.filterUser)
}
